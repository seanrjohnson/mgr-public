MGR (Mint Genomics Resource) is a content management system for genomic and transcriptomic data. As implied by the name, it was originally built to house data for mint plants, but it can be easily adapted for data from other organisms. I made MGR because I couldn't figure out how to get Tripal to behave how I wanted it to. I later found out that was my fault, not Tripal's fault, so if you want a more or less feature complete genomics content management system, look there. If you want a simple system written in Python (Django), or you have the misfortune of having a Windows server (as I do), then MGR might be just the thing for you (but it still might be worth your while to shoot the Tripal people an e-mail, just to make sure it really can't handle your situation).

For installation instructions see design_docs/windows_apache_setup.txt. For the most part, content management is done through the Administrative interface. However, several source files have to be edited directly:  
mgr_main/templates/mgr_main/base.html : <h1>DATABASE NAME</h1>, <!-- FOOTER LOGO -->, <!-- FOOTER TEXT -->  
mgr_main/templates/mgr_main/analytics.html : include analytics javascript to be run on every page, for example from google analytics.  
mgr_main/templates/mgr_main/contact.html : add your contact information  

MGR was written by Sean Johnson, at Washington State University. It is provided without any warranty of any kind, I am not responsible if its use causes you to become run over by an elephant etc. etc.

The white_wall_hash.png background image is from http://subtlepatterns.com