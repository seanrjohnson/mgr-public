from .base import FunctionalTestBase
import mgr_main.test_data as test_data

class HomePageTest(FunctionalTestBase):
    
    
    def test_homepage_layout(self):
        #Open homepage
        self.browser.get(self.server_url)
        #header and footer are as they should be
        self.check_header_and_footer()
        
        # there's a general information section
        gen_inf = self.browser.find_element_by_id("mgr_main_home_description")
        
        #there should be a news section with news as the heading
        news = self.browser.find_element_by_id("mgr_main_home_news")
        title = news.find_element_by_css_selector('.panel-title')
        self.assertEqual(title.text, 'News')
        
    def test_description_default(self):
        test_data.load_default_homepage()
        
        #Open homepage
        self.browser.get(self.server_url)
        
        # #there's a news section and a general information section
        gen_inf = self.browser.find_element_by_id("mgr_main_home_description")
        
        self.assertIn("e called glandular trichomes. The econ", gen_inf.text)
        
    def test_news_default(self):
        test_data.load_default_homepage()
        
        #Open homepage
        self.browser.get(self.server_url)
        
      
        #there should be a news section with news as the heading
        news = self.browser.find_element_by_id("mgr_main_home_news")
        self.assertTextOrder(news.text, "Feb. 10, 2014", "Peppermint added to species", "Feb. 7, 2014", "The Mint Genomics Resource is online!")
        # news1 = news.text.find("Peppermint added to species")
        # news2 = news.text.find("The Mint Genomics Resource is online!")
        # #both should be found in the html
        # self.assertNotEqual(news1, -1)
        # self.assertNotEqual(news2, -1)
        # #news2 should come after news1
        # self.assertGreater(news2, news1)