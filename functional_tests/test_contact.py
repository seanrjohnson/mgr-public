from .base import FunctionalTestBase
import mgr_main.test_data as test_data

class ContactTest(FunctionalTestBase):
    def test_contact_page(self):
        #Open homepage
        self.browser.get(self.server_url)
        
        #click "Contact" button
        self.navigate_to_new_page("Contact")
        page = self.browser.find_element_by_id("mgr_main_contact")
        self.assertIn("Project lead: Mark Lange", page.text)
        self.assertIn("Developer and webmaster: Sean Johnson", page.text)
        self.assertIn("See also: the Lange laboratory main website", page.text)