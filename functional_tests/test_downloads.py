from .base import FunctionalTestBase
import mgr_main.test_data as test_data

class DownloadsTest(FunctionalTestBase):
    def test_downloads_page(self):
        test_data.load_default_downloads()
        #Open homepage
        self.browser.get(self.server_url)
        
        #click "Downloads" button
        self.navigate_to_new_page("Downloads")
        
        #observe that there are some downloads available 
        page = self.browser.find_element_by_id("mgr_main_downloads")
        self.assertTextOrder(page.text, "Expressed Sequence Tags", "mRNA assemblies", "Short Reads")
        
        #TODO: need some way to check that download links work