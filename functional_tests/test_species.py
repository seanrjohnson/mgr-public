from .base import FunctionalTestBase
import mgr_main.test_data as test_data

class SpeciesTest(FunctionalTestBase):
    def test_peppermint_page(self):
        test_data.load_peppermint_page()
        #Open homepage
        self.browser.get(self.server_url)
        
        #navigate to peppermint page
        self.click_button("Species")
        self.click_button("Mentha x piperita (peppermint)")
        
        #verify that the correct buttons are active
        self.click_button("Species") #open the menu again
        self.assert_button_active("Mentha x piperita (peppermint)")
        self.assert_button_active("Species")
        self.click_button("Species") #close the menu
        
        links = self.browser.find_element_by_id("mgr_main_species_links")
        
        self.assertIn("USDA plant profile", links.text)
        self.assertIn("Wikipedia page", links.text)
        
        #self.assertIn("Downloads", links.text) #TODO: once downloads are working, test that this actually goes there
        #self.assertIn("Terpenoid pathway", links.text) #TODO: once pathway page is working, test that this actually goes there
        #self.assertIn("Gene expression", links.text) #TODO: once gene expression page is working, test that this actually goes there
        
        main = self.browser.find_element_by_id("mgr_main_species")
        self.assertIn("commercially by steam distillation and is rich",main.text)
        