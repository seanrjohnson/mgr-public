from selenium import webdriver
from selenium.common import exceptions as selenium_exceptions
from django.test import LiveServerTestCase
#from selenium.webdriver.common.keys import Keys
import sys
import time
import filecmp

class FunctionalTestBase(LiveServerTestCase):
    '''
        Inspired by and/or copied from Test-Driven Web Development with Python by Harry Percival
    '''
    @classmethod
    def setUpClass(cls):
        for arg in sys.argv:
            if 'liveserver' in arg:
                cls.server_url = 'http://' + arg.split('=')[1]
                return
        LiveServerTestCase.setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            LiveServerTestCase.tearDownClass()
    
    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)
        
    def tearDown(self):
        self.browser.quit()
    
    
    def assertTextOrder(self, base_text, *args):
        '''
            all parameters are strings
            
            asserts that the strings in *args all occur in base_text, and occur in the same order as in *args
        '''
    
        arg_position = list()
        for arg in args:
            arg_position.append(base_text.find(arg))
        
        #assert that the texts were fount
        for pos in arg_position:
            self.assertNotEqual(pos, -1)
        
        #assert that they are in the correct order 
        self.assertEqual(arg_position, sorted(arg_position))
        
    def element_id_present(self, parent, id):
        ''' 
            adapted from
            http://stackoverflow.com/questions/17411671/checking-if-element-is-present-using-by-from-selenium-webdriver-and-python
        '''
        try:
            elem = parent.find_element_by_id(id)
        except selenium_exceptions.NoSuchElementException as e:
            return False
        return True
        
    def button_with_text_exists(self, parent, link_text):
        try:
            elem = parent.find_element_by_link_text(link_text)
        except selenium_exceptions.NoSuchElementException as e:
            return False
        return True
    
    def click_button(self, button_link_text):
        
        try:
            button = self.browser.find_element_by_link_text(button_link_text)
        except:
            #if it there isn't a link, maybe there's an actual button
            button = self.browser.find_element_by_xpath("//button[contains(text(),'%s')]" % button_link_text)
        button.click()
    
    def navigate_to_new_page(self, page):
        
        #click the page button in the title
        self.click_button(page)
        
        #select active menu button
        active_menu_button = self.browser.find_element_by_css_selector("#mgr_main_main_menu_bar .active")
        self.assertIn(page, active_menu_button.text.split())
    
    def assert_button_active(self, button_link_text):
        button = self.browser.find_element_by_link_text(button_link_text)
        
        active_menu_buttons = self.browser.find_elements_by_css_selector(".active")
        first_lines = [x.text.split('\n')[0] for x in active_menu_buttons] #this presupposes single-line text elements, but I think that should be ok.
        self.assertIn(button_link_text, first_lines) 
    
    def check_header_and_footer(self):
        #Page title
        self.assertIn('Mint Genomics Resource', self.browser.title)
        
        #Header
        header = self.browser.find_element_by_id('header')
        #header text
        self.assertIn("Mint Genomics Resource", header.text)
        #header picture
        # self.assertTrue(self.element_id_present(header, 'mgr_banner'))
        # image = header.find_element_by_id('mgr_banner')
        # self.browser.execute_script("return (typeof arguments[0].naturalWidth!=\"undefined\" && arguments[0].naturalWidth>0)", image)
        #menu
        menu = self.browser.find_element_by_css_selector(".nav")
        for button in ('Home', 'Species', 'Downloads', 'Tools', 'Contact'):
            self.assertTrue(self.button_with_text_exists(menu, button))
        for button in ('Blast', 'aardvark', 'doggie'): #control: these things should not be there
            self.assertFalse(self.button_with_text_exists(menu, button))
        
        #Footer
        footer = self.browser.find_element_by_id('footer')
        #copywrite this year in GMT
        self.assertIn(str(time.gmtime().tm_year), footer.text)
        #Hosting notice
        self.assertIn("Mint Genomics Resource is developed and hosted by the Lange laboratory at WSU, all linked data and resources are property of their respective owners", footer.text)
        #wsu logo
        self.assertTrue(self.element_id_present(footer, 'wsu_logo'))
        image = footer.find_element_by_id('wsu_logo')
        self.browser.execute_script("return (typeof arguments[0].naturalWidth!=\"undefined\" && arguments[0].naturalWidth>0)", image) #adapted from http://watirmelon.com/2012/11/27/checking-an-image-is-actually-visible-using-webdriver/
        #legal notice
        self.assertIn('as-is', footer.text)
