from .base import FunctionalTestBase
import mgr_main.test_data as test_data
from selenium.webdriver.common.keys import Keys

class BlastTest(FunctionalTestBase):
    def test_blast_page(self):
        #Open homepage
        self.browser.get(self.server_url)
        
        #open Blast page
        self.click_button("Tools")
        self.click_button("Blast")
        
        #verify that the correct buttons are active
        self.click_button("Tools") #open the menu again
        self.assert_button_active("Tools")
        self.assert_button_active("Blast")
        self.click_button("Tools") #close the menu
        
        #Blastn button should be active, and it should be possible to click-select other blasts to activate them:
        self.assert_button_active("blastn")
        self.click_button("blastp")
        self.assert_button_active("blastp")
        
        
        self.click_button("blastx")
        self.assert_button_active("blastx")
        self.click_button("tblastn")
        self.assert_button_active("tblastn")
        self.click_button("tblastx")
        self.assert_button_active("tblastx")
        self.click_button("blastx")
        self.assert_button_active("blastx")
        self.click_button("blastn")
        self.assert_button_active("blastn")
        

        inputbox = self.browser.find_element_by_id('id_query')
        inputbox.send_keys('>seq1\nGACTT')
        self.click_button("Submit")
        #TODO: now what?