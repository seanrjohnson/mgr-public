from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'mgr_main.views.home_page', name='main'),
    url(r'^contact$', 'mgr_main.views.contact', name='contact'),
    url(r'^downloads$', 'mgr_main.views.downloads', name='downloads'),
    url(r'^blast$', 'mgr_main.views.blast', name='blast'),
    url(r'^expression$', 'mgr_main.views.expression', name='expression'),
    url(r'^pathways$', 'mgr_main.views.pathways', name='pathways'),
    url(r'^pathways/(.+)$', 'mgr_main.views.pathways', name='pathways'),
    url(r'^species/(.+)', 'mgr_main.views.species', name='species')
)
