from django.db import models
from django.db.models.signals import post_init

_MAX_URL_LENGTH = 2048

class MgrModel(models.Model):
    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)
    
    class Meta:
        abstract = True

class HomepageNews(models.Model):
    name = models.CharField(max_length=100, default='news', help_text='name of the news item. Used in admin.')
    date = models.DateField(blank=False, help_text='When was the news posted?')
    content = models.TextField(help_text="The text of the news item (links and html aren't currently allowed, they will be rendered in plain text, but this could be changed).")
    
    
    class Meta:
        verbose_name = "news"
        verbose_name_plural = "news"
    
class HomepageDescription(models.Model):
    name = models.CharField(max_length=100, default='description', help_text='name of the description item. Used in admin.')
    content = models.TextField(help_text="The text of the description item (links and html aren't currently allowed, they will be rendered in plain text, but this could be changed).")
    order = models.IntegerField(unique=True, help_text='What order it shows up in the list of descriptions on the homepage. Lower means closer to the top of the list. Negative numbers ok.')
    class Meta:
        verbose_name = "description"

class Content(models.Model):
    '''
        Note: don't make instances of this model, use one of its children instead.
        Most of the time, children should have a page_name field.
    '''
    content_html = models.TextField(help_text='HTML for the body of the page. It will be rendered as a Django template, so template language directives are also legal')
    internal_name = models.CharField(max_length=50, blank=False, unique=True, editable=False, help_text='Used to connect link records to content page records.') #what you use to connect link records to content page records
    def __str__(self):
        return self.internal_name
    
class Species(Content):
    page_name = models.CharField(max_length=25, blank=False, unique=True, help_text='species part of the url')
    genus_name = models.CharField(max_length=50, blank=False)
    species_name = models.CharField(max_length=50, blank=False)
    common_name = models.CharField(max_length=100)
    full_name = models.CharField(max_length=205, editable=False)
    
    def save(self, *args, **kwargs):
        self.internal_name = "species_" + self.page_name #TODO: test this
        self.full_name = str(self)
        super(Content, self).save(*args, **kwargs)
    def __str__(self):
        if self.common_name != "":
            out = "%s %s (%s)" % (self.genus_name, self.species_name, self.common_name)
        else:
            out = "%s %s" % (self.genus_name, self.species_name)
        return out
    
    class Meta:
        unique_together = ("genus_name", "species_name")
        verbose_name = "species"
        verbose_name_plural = "species"

class Download(models.Model):
    heading = models.CharField(max_length=1000, blank=False,  help_text='What category of download is it? It will appear under this bold heading on the downloads page, in a list with other downloads with the same heading')
    name = models.CharField(max_length=1000, blank=False, help_text='Name of the download. Will appear in the leftmost column of the table. Could just be the filename.')
    target = models.CharField(max_length=_MAX_URL_LENGTH, blank=False, help_text='url or Django static directive for the location of the file itself.')
    description = models.TextField(help_text='will appear in the middle column of the table.')
    owner = models.TextField(help_text='Who generated or owns the data? Will appear in the table')
    reference = models.TextField(help_text='Text for the reference column')
    reference_target = models.CharField(max_length=_MAX_URL_LENGTH, blank=True, help_text='link for the reference column')
    
    class Meta:
        unique_together = ("heading", "name")


class Link(MgrModel):
    parent = models.ForeignKey(Content, help_text='The content page where this link is attached') #TODO: make this a many_to_many (?)
    text = models.CharField(max_length=5000, help_text='text of the link')
    target = models.CharField(max_length=_MAX_URL_LENGTH, help_text='where the link points to')
    order = models.IntegerField()
    
    class Meta:
        unique_together = ("parent", "order")

class Pathway(Content):
    page_name = models.CharField(max_length=25, blank=False, unique=True, help_text='pathway part of the url')
    category = models.CharField(max_length=100, blank=False, help_text='heading under which to put it on the pathways index page')
    pathway_name = models.CharField(max_length=100, blank=False)
    species = models.ManyToManyField(Species, blank=True, help_text='Which species is this pathway found in?') #Note: so far no functionality is dependent on this property, but eventually it might be used for generating automatic links between the species and pathway
    def save(self, *args, **kwargs):
        self.internal_name = "pathway_" + self.page_name #TODO: test this
        self.full_name = str(self)
        super(Content, self).save(*args, **kwargs)

class Experiment(MgrModel):
    name=models.CharField(max_length=500, blank=False)
    description=models.TextField(blank=False)

class Sequence(MgrModel):
    parent=models.ForeignKey(Experiment, blank=False, help_text='Experiment that this sequence was generated from')
    name=models.CharField(max_length=500, blank=False)
    value=models.TextField(blank=False, help_text='the bases or amino acids of the sequence')
    
    class Meta:
        unique_together = ("parent", "name")

class Annotation(MgrModel):
    parent=models.ForeignKey(Sequence, blank=False, help_text='Sequence that the annotation describes')
    type=models.CharField(max_length=100, blank=False, help_text='property that the annotation describes')
    value=models.CharField(max_length=1000, blank=True)
    class Meta:
        unique_together = ("parent", "type") # this is a bit restrictive. my suggestion if you want more than one annotation of a given type, encode it in the string somehow (like semicolon separated or something)
    
class BlastDataset(MgrModel):
    DATABASE_TYPES = (('N','nucleotide'), ('P','protein'))
    
    name=models.CharField(max_length=50, blank=False, unique=True)
    filename=models.CharField(max_length=100, blank=False, unique=True)
    type=models.CharField(max_length=1, blank=False, choices=DATABASE_TYPES)
    experiment=models.ForeignKey(Experiment, blank=True)
    sequence_name_regex=models.CharField(max_length=100, blank=False, default='^(.*)$')
    
class BlastQuery(MgrModel):
    MAX_HITS_CHOICES = ((1,1), (5,5), (10, 10), (50, 50), (100, 100), (500, 500))
    WORDSIZE_CHOICES = ((3, 3), (11, 11), (28, 28))
    MATRIX_CHOICES = ((x,x) for x in ('BLOSUM80', 'BLOSUM62', 'BLOSUM50', 'BLOSUM45', 'PAM250', 'BLOSUM90', 'PAM30', 'PAM70'))
    PROGRAM_CHOICES = ((x,x) for x in ('blastn','blastp','blastx','tblastn','tblastx'))
    
    program=models.CharField(max_length=7, choices=PROGRAM_CHOICES, blank=False, default='blastn', help_text='blast program to use')
    query=models.TextField(max_length=10000, blank=False, help_text='raw sequence or fasta of sequence to search')
    database=models.ForeignKey(BlastDataset, blank=False, help_text='model object describing the database on which this search is to be run')
    max_hits=models.SmallIntegerField(choices=MAX_HITS_CHOICES, blank=False, default=10, help_text='blast parameter, maximum number of hits to save')
    expect_value=models.CharField(max_length=25, blank=False, default='0.1', help_text='blast parameter e_value') #be careful with this one. Make sure you call blast securely
    word_size=models.SmallIntegerField(choices=WORDSIZE_CHOICES, blank=False, default=3, help_text='blast parameter word_size')
    matrix=models.CharField(max_length=10, choices=MATRIX_CHOICES, blank=False, default='BLOSUM62', help_text='blast parameter scoring matrix')
    
    search_finished=models.BooleanField(default=False, help_text='Becomes True when the search has finished')
    results=models.TextField(help_text='Full text of the results')
    expires=models.DateTimeField(help_text="It's ok to delete the sequence after this date")
