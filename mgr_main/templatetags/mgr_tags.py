from django import template
from django.template import Template, Context
from django.utils.safestring import mark_safe

register = template.Library()



def render_template(template_string):
    #TODO: wrap this in a try, catch that defaults to returning "" if there's an error
    try:
        template_string = "{% load staticfiles %}\n" + template_string # we want to allow the template to render static file links
        t = Template(template_string)
        c = Context()
        return t.render(c).strip()
    except:
        return ""
register.filter('render_template', render_template)


def render_species_name(species_model_object):
    """HTML rendering of a species name, with italics""" #TODO: Test this
    if species_model_object.common_name != "":
        out = "%s (<i>%s %s</i>)" % (species_model_object.common_name, species_model_object.genus_name, species_model_object.species_name)
    else:
        out = "<i>%s %s</i>" % (species_model_object.genus_name, species_model_object.species_name)
    return mark_safe(out)

register.filter('render_species_name', render_species_name)