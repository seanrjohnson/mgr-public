from django.contrib import admin
from mgr_main.models import HomepageNews, HomepageDescription, Species, Link, Download, Pathway

# Register your models here.
class HomepageItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'order']
    ordering = ['name', 'order']

class HomepageNewsAdmin(admin.ModelAdmin):
    list_display = ['name', 'date']
    ordering = ['name', 'date']
    
class LinkAdmin(admin.ModelAdmin):
    list_display = ['target','parent', 'order']
    ordering = ['parent', 'order']

class SpeciesAdmin(admin.ModelAdmin):
    list_display = ['page_name','genus_name', 'species_name', 'common_name']
    ordering = ['page_name', 'genus_name', 'species_name', 'common_name']

class DownloadsAdmin(admin.ModelAdmin):
    cats = ['name','heading']
    list_display = cats
    ordering = cats

class PathwaysAdmin(admin.ModelAdmin):
    cats = ['page_name','category','pathway_name']
    list_display = cats
    ordering = cats

admin.site.register(HomepageNews, HomepageNewsAdmin)
admin.site.register(HomepageDescription, HomepageItemAdmin)
admin.site.register(Link, LinkAdmin)
admin.site.register(Species, SpeciesAdmin)
admin.site.register(Download, DownloadsAdmin)
admin.site.register(Pathway, PathwaysAdmin)