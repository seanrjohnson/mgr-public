from django.forms import ModelForm
from mgr_main.models import BlastQuery

class BlastFormBase(ModelForm):
    class Meta:
        model = BlastQuery
        fields = ['query', 'database', 'max_hits', 'expect_value', 'word_size', 'matrix']
        help_texts = {x:"" for x in fields} #turn off the help text