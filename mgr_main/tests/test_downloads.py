from .base_test import BaseTest, test_data, soup

class DownloadsTest(BaseTest):
    
    def test_downloads_page(self):
        test_data.load_default_downloads()
        res = self.client.get('/downloads')
        bs = soup(res.content)
        
        #observe that there are some downloads available 
        page = bs.select("#mgr_main_downloads")[0]
        self.assertTextOrder(str(page), "Expressed Sequence Tags", "mRNA assemblies", "Short Reads")
        
        sr = page.find("div", {'class':"panel-heading"}, text="Short Reads").parent
        download_links = "\n".join([str(x) for x in sr.find_all("a")])
        self.assertTextOrder(download_links, "Erospicata", "Mentha aquatica", "Mentha arvensis", "Mentha spicata", "Mentha x piperita")
        self.assertTextOrder(str(sr), "File", "Description", "Owner", "Reference")
        self.assertTextOrder(str(sr), "Erospicata ", "mRNA isolated from glandular trichomes of Erospicata", "Lange laboratory", "unpublished")