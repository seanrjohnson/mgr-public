from .base_test import BaseTest, test_data
from django.template.loader import render_to_string
from mgr_main.models import HomepageNews, HomepageDescription, Species, Link

class ModelTest(BaseTest):
    # def test_home_page_data_model(self):
        # #self.homepage_item(HomepageNews)
        # self.homepage_item(HomepageDescription)
    
    # def homepage_item(self, model_module):
        # saved_items = model_module.objects.all()
        # starting_count = saved_items.count()
        # item = model_module()
        # item.content = "A line of text"
        # item.order = starting_count + 1
        # item.save()
        # self.assertEqual(saved_items.count(), starting_count + 1)
        # saved_item = saved_items[starting_count]
        # self.assertEqual(item.content, "A line of text")
        # self.assertEqual(item.order, starting_count+1)
    
    def test_homepage_description(self):
        model_module = HomepageDescription
        saved_items = model_module.objects.all()
        starting_count = saved_items.count()
        item = model_module()
        item.content = "A line of text"
        item.order = starting_count + 1
        item.save()
        self.assertEqual(saved_items.count(), starting_count + 1)
        saved_item = saved_items[starting_count]
        self.assertEqual(item.content, "A line of text")
        self.assertEqual(item.order, starting_count+1)
    
    def test_homepage_news(self):
        model_module = HomepageNews
        saved_items = model_module.objects.all()
        starting_count = saved_items.count()
        item = model_module()
        item.content = "A line of text"
        item.date = '2016-06-03'
        item.save()
        self.assertEqual(saved_items.count(), starting_count + 1)
        saved_item = saved_items[starting_count]
        self.assertEqual(item.content, "A line of text")
        self.assertEqual(item.date, '2016-06-03')
    
    def add_link(self, parent, link_text, link_target, link_order):
        link = Link()
        link.parent = parent
        link.text = link_text
        link.target = link_target
        link.order = link_order
        link.save()
    
    def test_species(self):
        s1 = Species()
        s1.internal_name = "peppermint"
        s1.genus_name = "Mentha"
        s1.species_name = "x piperita"
        s1.common_name = "peppermint"
        s1.page_name = "peppermint"
        s1.content_html = "example text"
        s1.save()
        
        targets = ('{% url "mgr_main.views.home_page" %}', "http://plants.usda.gov/core/profile?symbol=MEPI", "http://en.wikipedia.org/wiki/Peppermint")
        texts = ("homepage", "USDA plant profile", "Wikipedia page")
        for i in (1,2,0):
            self.add_link(s1, texts[i], targets[i], i)
        
        saved = Species.objects.all()
        s1_loaded = saved[len(saved)-1]
        
        self.assertEqual(s1_loaded, s1)
        links = s1.link_set.all()
        l = [links.get(order=x) for x in range(0,3)]
        
        for i in (1,2,0):
            self.assertEqual(l[i].text, texts[i])
            self.assertEqual(l[i].target, targets[i])
        
        self.assertEqual(s1_loaded.full_name, "Mentha x piperita (peppermint)")
        s1_loaded.common_name = ""
        self.assertEqual(s1_loaded.full_name, "Mentha x piperita (peppermint)")
        s1_loaded.save()
        self.assertEqual(s1_loaded.full_name, "Mentha x piperita")
        
    def test_download(self):
        pass
        #I feel like this is a stupid test, but I can't think of a better one
        # don't bother....
        
        # headings = ('h1', 'h2', 'h3')
        # links = ('l1', 'l2', 'l3')
        # descriptions = ('d1', 'd2', 'd3')
        
        # for i in range(len(headings)):
            # d = Downolad()
            # d.heading = headings(i)
            # d.link = links(i)
            # d.description = descriptions(i)
            # d.save()
        
        # d_list = Downloads.objects.all().sortby('heading')
        
        # self.assertEqual(len(d_list), 3)
        # for i in range(len(d_list)):
            # self.AssertEqual(d_list[i].heading, headings[i])
            # self.AssertEqual(d_list[i].link, links[i])
            # self.AssertEqual(d_list[i].description, descriptions[i])
        
        
    def test_pathway(self):
        pass
    
    def test_blast(self):
        pass
    

