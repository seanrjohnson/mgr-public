﻿from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from mgr_main.models import HomepageNews, HomepageDescription, Species, Link

import mgr_main.test_data as test_data

from bs4 import BeautifulSoup as soup #for testing response html
#from pprint import pprint as pprint

class BaseTest(TestCase):
    
    def assertTextOrder(self, base_text, *args):
        '''
            all parameters are strings
            
            asserts that the strings in *args all occur in base_text, and occur in the same order as in *args
        '''
    
        arg_position = list()
        for arg in args:
            arg_position.append(base_text.find(arg))
        
        #assert that the texts were fount
        for (i, pos) in enumerate(arg_position):
            self.assertNotEqual(pos, -1, msg="string %s not found in text %s" % (args[i], base_text))
        
        #assert that they are in the correct order 
        self.assertEqual(arg_position, sorted(arg_position), "strings \n %s \n in wrong order in text \n %s" % ("\n".join(args), base_text))

