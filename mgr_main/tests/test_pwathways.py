from .base_test import BaseTest, test_data, soup

class PathwaysTest(BaseTest):
    
    def test_pathways_page(self):
        test_data.load_spearmint_terpenoid_pathway()
        res = self.client.get('/pathways')
        bs = soup(res.content)
        
        #observe that there are some pathways available 
        page = bs.select("#mgr_main_pathways")[0]
        self.assertTextOrder(str(page), "Terpenoid Biosynthesis", "Spearmint Monoterpenoid Biosynthesis")
        
    
    def test_spearmint_terpene_page(self):
        test_data.load_spearmint_terpenoid_pathway()
        res = self.client.get('/pathways/m_spicata_terpene')
        bs = soup(res.content)
        
        page = bs.select("#mgr_main_specific_pathway")[0]
        self.assertTextOrder(str(page), "Proposed terpenoid", "Gene/enzyme annotation", "Geranyl diphosphate synthase")
        

