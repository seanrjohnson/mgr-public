from .base_test import BaseTest, test_data, soup

class SpeciesTest(BaseTest):
    
    def test_peppermint_page(self):
        test_data.load_peppermint_page()
        res = self.client.get('/species/Peppermint')
        bs = soup(res.content)
        self.assertIn("USDA plant profile", bs.text)
        self.assertIn("Downloads", bs.text)
        self.assertIn("The most devastating peppermint pathogen is Verticillium wilt", bs.text)
        
        self.assertTextOrder(bs.text, "USDA plant profile", "Wikipedia page")