from .base_test import BaseTest, test_data, soup
from django.template.loader import render_to_string
import mgr_main.views as mgr_main_views

def strip_html(text):
    '''
        removes all spaces, tabs, newlines, and carriage returns from a string
    '''
    out = text.replace(" ","")
    out = out.replace("\n","")
    out = out.replace("\r","")
    return out

class NavigationTest(BaseTest):
    
    def test_contact_page(self):
        res = self.client.get('/contact')
        bs = soup(res.content)
        main = bs.select("#main_div")[0]
        rendered = render_to_string('mgr_main/contact.html', mgr_main_views._base_dict("Contact"))
        self.assertIn(strip_html(str(main)), strip_html(rendered))
    
    def test_downloads_page(self):
        res = self.client.get('/downloads')
        bs = soup(res.content)
        main = bs.select("#main_div")[0]
        rendered = render_to_string('mgr_main/downloads.html', mgr_main_views._base_dict("Downloads"))
        self.assertIn(strip_html(str(main)), strip_html(rendered))
        
    
    def test_blast_page(self):
        #TODO: redo this (or don't because it's already covered in the functional test)
        # res = self.client.get('/blast')
        # bs = soup(res.content)
        # main = bs.select("#main_div")[0]
        # params = mgr_main_views._base_dict("Blast")
        # params['page'].append('Tools')
        # rendered = render_to_string('mgr_main/blast.html', params)
        # self.assertIn(strip_html(str(main)), strip_html(rendered))
        pass
        