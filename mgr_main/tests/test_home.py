from .base_test import BaseTest, test_data, soup, resolve
import mgr_main.views as mgr_main_views
import time
from mgr_main.views import home_page

class HomePageTest(BaseTest):
    
    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home_page)
    
    def test_base_dict_generation(self):
        page = "Home"
        d = mgr_main_views._base_dict("Home")
        self.assertEqual(d['year'], str(time.gmtime().tm_year))
        self.assertEqual(d['page'], ['Home'])
    
    def test_add_news(self):
        mid_n = test_data.new('HomepageNews', content="middle news", date="2012-01-05")
        bottom_n = test_data.new('HomepageNews', content="bottom news", date="2011-01-05")
        top_n = test_data.new('HomepageNews', content="top news", date="2013-01-05")
        
        res = self.client.get('/')
        
        bs = soup(res.content)
        
        news = bs.select('#mgr_main_home_news')[0]
        
        news_index = dict()
        news_index['mid'] = news.text.find("middle news")
        news_index['bottom'] = news.text.find("bottom news")
        news_index['top'] = news.text.find("top news")
        
        for i in news_index:
            self.assertNotEqual(news_index[i], -1)
        self.assertGreater(news_index['mid'], news_index['top'])
        self.assertGreater(news_index['bottom'], news_index['mid'])
    
    def test_add_description(self):
        mid_d = test_data.new('HomepageDescription', content="middle description", order=12)
        bottom_d = test_data.new('HomepageDescription', content="bottom description", order=13)
        top_d = test_data.new('HomepageDescription', content="top description", order=11)
        
        
        res = self.client.get('/')
        
        bs = soup(res.content)
        
        desc = bs.select('#mgr_main_home_description')[0]
        
        self.assertTextOrder(desc.text, "top description", "middle description", "bottom description")

