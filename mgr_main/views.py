from django.shortcuts import render
from django.template import Template, Context
from django.template.loader import render_to_string
import time
from mgr_main.models import HomepageNews, HomepageDescription, Species, Download, Pathway
import mgr_main.forms as forms

BLAST_TYPES = set(('blastn','blastp','blastx','tblastn','tblastx'))

def _prerender_template(template_string, context={}):
    #TODO: wrap this in a try, catch that defaults to returning "" if there's an error. Also, refactor to remove either this or the template tag version of it. Both are probably not necessary.
    #try:
        template_string = "{% load staticfiles %}\n{% load mgr_tags %}\n" + template_string # we want to allow the template to render static file links and species names etc
        # with open("out.log", "wb") as f:
            # f.write(template_string.encode('utf-8'))
        t = Template(template_string)
        c = Context(context)
        # with open("out2.log", "wb") as f:
            # f.write(out.encode('utf-8'))
        return t.render(c)
    #except:
    #    return ""

def _base_dict(page):
    species = Species.objects.all().order_by("full_name")
    return {'year': str(time.gmtime().tm_year), 'page':[page,], 'species_list':species}

def home_page(request):
    #import mgr_main.test_data as test_data
    #test_data.load_default_downloads()
    # test_data.load_default_homepage()
    # test_data.load_peppermint_page()
    # test_data.load_spearmint_terpenoid_pathway()
    
    
    template_vars = _base_dict('Home')
    news = HomepageNews.objects.all().order_by("-date")
    template_vars['news_list'] = news
    descriptions = HomepageDescription.objects.all().order_by("order")
    template_vars['description_list'] = descriptions
    return render(request, 'mgr_main/home.html', template_vars)
    

def contact(request):
    template_vars = _base_dict('Contact')
    return render(request, 'mgr_main/contact.html', template_vars)

def downloads(request):
    template_vars = _base_dict('Downloads')
    template_vars['headings'] = sorted(Download.objects.values_list('heading', flat=True).distinct(), key=lambda x: x.upper())
    template_vars['downloads'] = Download.objects.all().order_by('name')
    return render(request, 'mgr_main/downloads.html', template_vars)

def blast(request):
    '''
        If there is a post, process the POST into a blast query and redirect to the blast query page. Or if there are errors, render the errors
        If there is a GET, render the form page
    '''
    template_vars = _base_dict('Tools')
    template_vars['page'].append('Blast')
    
    prog=request.GET.get('prog','blastn')
    if (not (prog in BLAST_TYPES)):
        prog='blastn'
    blast_form = forms.BlastFormBase()
    template_vars['form'] = blast_form
    template_vars['prog'] = prog
    return render(request, 'mgr_main/blast.html', template_vars)

def blastquery(request, query=None):
    '''
        if query is not defined redirect to blast. If it is defined, post the results, or the waiting page
    '''
    pass
    
    
def expression(request):
    template_vars = _base_dict('Tools')
    template_vars['page'].append('Expression')
    return render(request, 'mgr_main/expression.html', template_vars)
    
def pathways(request, pathway=None):
    template_vars = _base_dict('Tools')
    template_vars['page'].append('Pathways')
    
    if (pathway is None) or pathway == "":
        template_vars['headings'] = sorted(Pathway.objects.values_list('category', flat=True).distinct(), key=lambda x: x.upper())
        template_vars['pathways'] = Pathway.objects.all().order_by('pathway_name')
        return render(request, 'mgr_main/pathways.html', template_vars)
    else: #TODO: throw a 404 if path not valid
        template_vars['pathway'] = Pathway.objects.all().get(page_name=pathway) #cut off the / at the beginning
        template_vars['link_set'] = template_vars['pathway'].link_set.all().order_by('order')
        template_vars['content'] = _prerender_template(template_vars['pathway'].content_html)
        #template_vars['content'] = template_vars['pathway'].content_html
        return render(request, 'mgr_main/specific_pathway.html', template_vars)
        
def species(request, species_page): #TODO: throw a 404 if path not valid
    template_vars = _base_dict('Species')
    template_vars['page'].append(species_page)
    template_vars['species'] = Species.objects.get(page_name=species_page)
    template_vars['link_set'] = template_vars['species'].link_set.all().order_by('order')
    template_vars['species_content'] = _prerender_template(template_vars['species'].content_html, template_vars)
    return render(request, 'mgr_main/species.html', template_vars)